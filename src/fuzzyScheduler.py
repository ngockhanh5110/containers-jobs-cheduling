import pandas as pd
from src.cspProblem import CSP, Constraint
from src.cspSearch import Search_from_CSP, Arc
from src.searchGeneric import Searcher, AStarSearcher
from datetime import datetime
from src.display import Displayable, visualize
from src.searchProblem import Path
import datetime
import sys 
import csv
import pandas as pd
import openpyxl



#! This class defined comparision operators
class operatorsConstraints:
    def __init__(self):
        pass

    def diff(self, x, y):
        # There is no conflicting container between the two
        for i in x:
            if i in y: return False
        return True

    def weight(self, weight):
        # The weight of the two container cannot go over Trailer maximum load
        return lambda x: (x[0].weight + x[1].weight) <= weight

    def calCost(self, node,var, val):
        # The cost should consider `when` as well.
        x,y = (val[0],val[1])
        if abs(x.priority - y.priority) > 1: 
            priority = (x.priority + y.priority) -  abs(x.priority - y.priority)  + 0.1*abs(x.priority - y.priority) 
            count = 1
        elif abs(x.priority - y.priority) == 1: 
            priority = (x.priority + y.priority) -  abs(x.priority - y.priority)
            count = 1
        else: 
            priority = (x.priority + y.priority)
            count = 0
            for i in [x,y]:
                if i.no == "NULL": count +=1
        return var[1] * priority + 1.5* count - (x.weight+ y.weight)/1000000 - 10000*len(node)

#! This class inherent from CSP class
class fuzzyCSP(CSP):
    def __init__(self, domains, containers):
        self.op = operatorsConstraints()
        self.constraints = self.initializeConstraints(domains)
        self.containers = containers
        super().__init__(domains, self.constraints)

    def initializeConstraints(self, domains, constraints = []):
        """
        A few rules has to be followed:
        1. Each container, in Tuple of Containers, exists in one slot cannot be found in other slots
        2. Total weight cannot go over maximum Trailer's load
        """
        # Rule 1:
        constraints = []
        lts = list(domains.keys())
        n = len(domains.keys())
        for i in range(0,n-1):
            for j in range(i+1,n):
                constraints.append(Constraint((lts[i],lts[j]), self.op.diff))

        # Rule 2:
        for i in domains.keys():
            constraints.append(Constraint((i,), self.op.weight(i[0].load)))
        return constraints

#! This class inherent from Search_from_CSP class
class Search_from_fuzzyCSP(Search_from_CSP):
    def __init__(self, csp, variables_order):
        super().__init__(csp, variables_order)
        self.takenVar = {i: False for i in variables_order}

    def dict_union(self, d1, d2):
        """returns a dictionary that contains the keys of d1 and d2.
        The value for each key that is in d2 is the value from d2,
        otherwise it is the value from d1.
        This does not have side effects.
        """
        d = dict(d1)    # copy d1
        d.update(d2)
        return d

    def is_goal(self, node):
        if len(node)==len(self.csp.variables): 
            return True
        lts = []
        for v in node.values():
            for i in range(0,2):
                if v[i].no != "NULL" : lts.append(v[i].no)
        if len(lts) == len(self.csp.containers): return True
        else: return False

    def neighbors(self, node):
        """returns a list of the neighboring nodes of node.
        """
        res = []
        var = self.variables[len(node)]  # the next variable
        self.takenVar[var] = True
        for val in self.csp.domains[var]:
            new_env = self.dict_union(node, {var: val})  # dictionary union
            if self.csp.consistent(new_env):
                cost = self.csp.op.calCost(node, var, val)
                res.append(Arc(node, new_env, cost=cost))
        if len(res) == 0: res.append(Arc(node, self.dict_union(node, {var: (Container("NULL","NULL",0,"NULL","NULL",0,0),
                                                                            Container("NULL","NULL",0,"NULL","NULL",0,0))}), cost=0))
        return res

#! This class inherent from AStarSearcher class to modify search method
class AStarSearcherFuzzy(AStarSearcher):
    def __init__(self, problem):
        super().__init__(problem)

    @visualize
    def search(self):
        """returns (next) path from the problem's start node
        to a goal node. 
        Returns None if no path exists.
        """
        out = {}
        while not self.empty_frontier() and not all(self.problem.takenVar.values()):
            path = self.frontier.pop()
            self.num_expanded += 1
            if self.problem.is_goal(path.end()):    # solution found
                self.solution = path   # store the solution found
                return path
            else:
                neighs = self.problem.neighbors(path.end())
                for arc in reversed(list(neighs)):
                    self.add_to_frontier(Path(path, arc))
        _ = self.frontier.pop()
        if len(_.end()) > len(path.end()): path = _
        return path

#! This function is used to processing raw text file

class Trailer:
    def __init__(self,loadcount, driver, truck, trailer,load, team = "TEAM 1"):
        self.loadcount = loadcount
        self.driver = driver
        self.truckno = truck 
        self.trailerno = trailer
        self.load = load
        self.team = team
    
    def __repr__(self):
        return self.trailerno

class Container: 
    def __init__(self, no, type, priority, cargo, desc, weight, load):
        self.no = no
        self.type = type
        self.priority = priority
        self.cargo = cargo
        self.desc = desc
        self.weight = weight
        self.load = load
    
    def __repr__(self):
        return str(self.no)


def readStocks(stockfile):
    containers = []
    df = pd.read_excel(stockfile,
                  dtype={'Container No.': str,
                         'Container Type' : str,
                         'Priority' : str,
                         'Cargo Type' : str,
                         'Description': str,
                         'Total Weight': float,
                         'Load': str})
    priority = {
        'P1': 1,
        'P2': 2,
        'P3': 3,
        'P4': 4,
    }
    load = {
        'S': 2,
        'D': 1,
        'Single': 2,
        'Double': 1,
    }
    for index, row in df.iterrows():
        containers.append(
            Container(
                row['Container No.'],
                row['Container Type'],
                priority[row['Priority']],
                row['Cargo Type'],
                row['Description'],
                row['Total Weight'],
                load[row['Load']]
            )
        )
    return containers

def readTrailers(trailerfile, count_):
    trailers = []
    df = pd.read_excel(trailerfile,
                  dtype={'DriverName': str,
                         'TruckNo' : str,
                         'TrailerNo' : str,
                         'Load': float,
                  })

    for index, row in df.iterrows():
        __ = count_
        if row['Load'] == 25:
            trailers.append(
                Trailer(
                    __,
                    row['DriverName'],
                    row['TruckNo'],
                    row['TrailerNo'],
                    row['Load'],
                    "TEAM 2"
                )
            )
        if row['Load'] == 25:
            trailers.append(
                Trailer(
                    __,
                    row['DriverName'],
                    row['TruckNo'],
                    row['TrailerNo'],
                    row['Load'],
                    "TANKER"
                )
            )
        else:
            trailers.append(
                Trailer(
                    __,
                    row['DriverName'],
                    row['TruckNo'],
                    row['TrailerNo'],
                    row['Load'],
                )
            )
        count_ +=1
    return trailers, count_

def dateHandler(start,end):
    start = datetime.datetime.strptime(start,'%d/%m/%Y')
    end = datetime.datetime.strptime(end,'%d/%m/%Y')
    daydict = {1: start.strftime('%m/%d/%Y')}
    count = 2
    next_date = start
    while next_date!=end:
        next_date += datetime.timedelta(days=1)
        daydict[count] = next_date.strftime('%m/%d/%Y')
        count+=1
    return daydict, list(range(1,count))

def solver(stockFile, trailerFile, tankerFile, start, end, totalFuel ):
    count_ = 1
    trailers,count_ = readTrailers(trailerFile,count_)
    containers = readStocks(stockFile)
    tanker,count_ = readTrailers(tankerFile,count_)
    daydict, dayrange = dateHandler(start,end)
    loaddict = {1: "Double", 2: "Single", 0 : ""}
    prioritydict = {
        1: 'P1',
        2: 'P2',
        3: 'P3',
        4: 'P4',
    }

    result = dict()
    undone = containers
    # print(len(undone))
    # print(dayrange)
    # print(daydict)
    for t in dayrange:
        #! This part for creating domain stored in _
        _ = set()
            
        for i in range(0,len(undone)-1):
            for j in range(i+1,len(undone)):
                if undone[i].load + undone[j].load == 2:
                    _.add((undone[i],undone[j]))
        for i in range(0,len(undone)):
            if undone[i].load == 2: _.add((undone[i],Container("NULL", "NULL", undone[i].priority, "NULL", "NULL", 0, 0)))
            else: _.add((undone[i],Container("NULL", "NULL", undone[i].priority + 0.5, "NULL", "NULL", 0, 0)))
        
        #! This part for executing the search engine
        variables_order = []
        domain = {}
        for k in trailers:
            variables_order.append((k,t))
            domain[(k,t)] = _
        
        csp10 = fuzzyCSP(domain, undone)
        searcher1 = AStarSearcherFuzzy(
            Search_from_fuzzyCSP(csp10, variables_order))
        temp = searcher1.search().end()
        # print(temp)
        # print(type(temp))
        result.update(temp)
        
        #! This part for updating the domain again.
        done = set()
        for k, v in result.items():
            if v[0].no != "NULL": done.add(v[0])
            if v[1].no != "NULL": done.add(v[1])
        undone = list(set(undone).difference(done))
        # print(len(undone))
        del csp10,searcher1,temp, domain

    # print("Done searching")
    #! This part will handle the fuel tanker
    countFuel = 0
    domainFuel = []
    for j in dayrange:
        for i in tanker:
            domainFuel.append((i,j))
    outputFuel = {}
    _ = True
    for i in domainFuel:
        if _:
            if (totalFuel - countFuel) >= i[0].load: outputFuel[i] = i[0].load
            else: outputFuel[i] = (totalFuel - countFuel)
            countFuel += i[0].load
            if countFuel >= totalFuel:
                _ = False
        else: 
            outputFuel[i] = 0

    #! This part printing the result
    data = [["Driver","Truck No","Trailer No","Load", "Date",
             "Container 1","Container 1 Weight", "Container 1 Load", "Priority", 
             "Container 2","Container 2 Weight","Container 2 Load","Priority",
             "Number of containers","Total Weight"]]

    def countContainers(a,b):
        count = 0
        for i in [a,b]:
            if i.no != "NULL": count += 1 
        return count 

    doneContainer = set()
    undoneContainer = set()
    for k, v in result.items():
        data.append([k[0].driver,k[0].truckno,k[0].trailerno,k[0].load,daydict[k[1]],
                     v[0],v[0].weight,loaddict[v[0].load],prioritydict[v[0].priority] if v[0].priority in prioritydict.keys() else "",
                     v[1],v[1].weight,loaddict[v[1].load],prioritydict[v[1].priority] if v[1].priority in prioritydict.keys() else "",
                     countContainers(v[1],v[0]),v[0].weight+ v[1].weight])

        if v[0].no != "NULL": doneContainer.add(v[0])
        if v[1].no != "NULL": doneContainer.add(v[1])
    undoneContainer = list(set(containers).difference(doneContainer))
    dataUndone = [['Container No.','Container Type' ,'Priority', 'Cargo Type','Description','Total Weight', 'Load']]
    dataDone = [['Container No.','Container Type' ,'Priority', 'Cargo Type','Description','Total Weight', 'Load']]
    dataContainer = [['Container No.','Container Type' ,'Priority', 'Cargo Type','Description','Total Weight', 'Load']]
    for i in undoneContainer:
        dataUndone.append([i.no, i.type, prioritydict[i.priority], i.cargo, i.desc, i.weight, loaddict[i.load]])
    for k in list(doneContainer):
        dataDone.append([k.no, k.type, prioritydict[k.priority], k.cargo, k.desc, k.weight, loaddict[k.load]])
    for z in list(containers):
        dataContainer.append([z.no, z.type, prioritydict[z.priority], z.cargo, z.desc, z.weight, loaddict[z.load]])
    dataTrailer = [["Driver name", "Truck no", "Trailer no", "Cap loaded"]]
    for i in trailers:
        dataTrailer.append([i.driver, i.truckno, i .trailerno, i.load])

    dataFuel = [["Driver","Truck No","Trailer No","Load", "Date"]]
    for k, v in outputFuel.items():
        dataFuel.append([k[0].driver,k[0].truckno,k[0].trailerno,v,daydict[k[1]]])

    dataTotalFuel = [['Total Fuel'],[totalFuel]]

    #! This part for data presenting purpose
    dataPresenting = [['Load No','Description','Data','Date','Team']]
    countLoad = 1
    for k, v in result.items():
        _ = k[0].loadcount
        team = k[0].team
        dataPresenting.append([_,'1. Driver',k[0].driver,daydict[k[1]],team])
        dataPresenting.append([_,'2. Truck No',k[0].truckno,daydict[k[1]],team])
        dataPresenting.append([_,'3. Trailer No',k[0].trailerno,daydict[k[1]],team])
        dataPresenting.append([_,'4. Load 1',v[0],daydict[k[1]], team])
        dataPresenting.append([_,'5. Load 2',v[1],daydict[k[1]], team])
        countLoad +=1
    for k, v in outputFuel.items():
        _ = k[0].loadcount
        team = k[0].team
        dataPresenting.append([_,'1. Driver',k[0].driver,daydict[k[1]],team])
        dataPresenting.append([_,'2. Truck No',k[0].truckno,daydict[k[1]], team])
        dataPresenting.append([_,'3. Tanker No',k[0].trailerno,daydict[k[1]], team])
        dataPresenting.append([_,'4. Load 1','DIESEL',daydict[k[1]], team])
        dataPresenting.append([_,'5. Load 2',v,daydict[k[1]], team])
        countLoad +=1

    #! This part for exporting to excel file
    pd.DataFrame(data[1:], columns=data[0]).to_excel('./results.xlsx', sheet_name= "Scheduling", index = False)
    with pd.ExcelWriter("./results.xlsx", engine='openpyxl', mode='a') as writer:
        pd.DataFrame(dataUndone[1:], columns= dataUndone[0]).to_excel(writer, sheet_name= "Undone Containers", index = False)
        pd.DataFrame(dataDone[1:], columns= dataDone[0]).to_excel(writer, sheet_name= "Done Containers", index = False)
        pd.DataFrame(dataContainer[1:], columns= dataContainer[0]).to_excel(writer, sheet_name= "All Containers", index = False)
        pd.DataFrame(dataTrailer[1:], columns= dataTrailer[0]).to_excel(writer, sheet_name= "All Trailers", index = False)
        pd.DataFrame(dataFuel[1:], columns= dataFuel[0]).to_excel(writer, sheet_name= "Tanker Scheduling", index = False)
        pd.DataFrame(dataTotalFuel[1:], columns= dataTotalFuel[0]).to_excel(writer, sheet_name= "Total Fuel", index = False)
        pd.DataFrame(dataPresenting[1:], columns= dataPresenting[0]).to_excel(writer, sheet_name= "Data presenting", index = False)
