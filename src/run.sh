#!/bin/bash

mkdir out
rm -v ./out/*
chmod u+x *
pip install -r requirements.txt
python3 fuzzyScheduler.py