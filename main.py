from tkinter import *
from tkinter import filedialog
import tkinter as tk
from src.fuzzyScheduler import solver as fz
import os
import datetime

class LabelEntry(tk.Frame):
    def __init__(self, parent, text, button=None):
        super().__init__(parent)
        self.pack(fill=tk.X)
        self.button = button
       

        lbl = tk.Label(self, text=text, width=14, anchor='w')
        lbl.pack(side=tk.LEFT, padx=5, pady=5)

        if self.button:
            frame2 = tk.Frame(self)
            frame2.pack(side=tk.LEFT, expand=True)

            self.entry = tk.Entry(frame2)
            self.entry.pack(side=tk.LEFT, fill=tk.X, padx=5)

            self.button.pack(in_=frame2, side=tk.LEFT, padx=5, pady=5)
        elif text == "Planned Fuel Ltrs":
            self.entry = tk.Entry(self)
            self.entry.pack(side=tk.LEFT, fill=tk.X, padx=5)
        else:
            self.entry = tk.Entry(self)
            self.entry.insert(0,"dd/MM/yyyy")
            self.entry.pack(side=tk.LEFT, fill=tk.X, padx=5)

    def browsingFile(self):
        pass

class App(tk.Tk):
    def __init__(self):
        super().__init__()
        self.start = ""
        self.end = ""
        self.trailerFile = ""
        self.stockFile = ""
        self.tankerFile  = ""
        self.fuel = ""
        self.title("Containers Scheduling Helper")

        frame = tk.Frame(self)
        frame.grid(row=0, column=0)

        self.entries = []
        for field in ['Container Stock', 'Trailers Allocated','Tankers Allocated', 'LineHaul Start date','LineHaul End date','Planned Fuel Ltrs']:
            if field ==  'Container Stock':
                button = tk.Button(text="Browse", command=  self.browseStock)
                self.entries.append(LabelEntry(frame, field, button))
            elif field ==  "Trailers Allocated":
                button = tk.Button(text="Browse", command= self.browseTrailer)
                self.entries.append(LabelEntry(frame, field, button))
            elif field ==  "Tankers Allocated":
                button = tk.Button(text="Browse", command= self.browseTanker)
                self.entries.append(LabelEntry(frame, field, button))
            else:
                self.entries.append(LabelEntry(frame, field))

        button1 = tk.Button(text="Submit", command= self.submitHandler).pack(in_=frame, side=tk.BOTTOM, padx=5, pady=5)

    def browseStock(self):
        self.stockFile = tk.filedialog.askopenfilename(initialdir = "./",
                                                    title = "Import stocks file")
        self.entries[0].entry.delete(0,10000)
        self.entries[0].entry.insert(0,self.stockFile)


    def browseTrailer(self):
        self.trailerFile = tk.filedialog.askopenfilename(initialdir = "./",
                                                    title = "Import trailers file")
        self.entries[1].entry.delete(0,10000)
        self.entries[1].entry.insert(0,self.trailerFile)

    def browseTanker(self):
        self.tankerFile = tk.filedialog.askopenfilename(initialdir = "./",
                                                    title = "Import tanker file")
        self.entries[2].entry.delete(0,10000)
        self.entries[2].entry.insert(0,self.tankerFile)

    def validateInput(self):
        
        try: 
            datetime.datetime.strptime(self.start,'%d/%m/%Y')
            datetime.datetime.strptime(self.end,'%d/%m/%Y')
        except ValueError:
            popup = tk.Tk()
            popup.title("!!!")
            tk.Label(popup, text = "The date inputs are in wrong format!").pack(side="top", fill="x", pady=10)
            
        if not os.path.isfile(self.trailerFile) or not os.path.isfile(self.stockFile) or not os.path.isfile(self.tankerFile) :
            if popup is not None:
                tk.Label(popup, text = "Input the data files").pack(side="top", fill="x", pady=10)
                return False
            else:
                popup = tk.Tk()
                popup.wm_title("!!!")
                tk.Label(popup, text = "Input the data files").pack(side="top", fill="x", pady=10)
                return False
        return True

    def submitHandler(self):
        self.start = self.entries[-3].entry.get()
        self.end = self.entries[-2].entry.get()
        self.fuel = float(self.entries[-1].entry.get().replace(',',''))
        if self.validateInput():
            fz(self.stockFile,self.trailerFile,self.tankerFile,self.start,self.end, self.fuel)
            self.quit()

if __name__ == "__main__":
    App().mainloop()

    #! This part for testing purpose
    # fz("src/data/stocks.xlsx",
    #    "src/data/trailers.xlsx",
    #    "src/data/tankers.xlsx",
    #    "1/1/2020",
    #    "9/1/2020", 
    #    float("2000000"))
    
